# Bloody Skak
(Skak is the Danish word for chess)

Bruh it's just like chess, but worse, and bloodier.
Drag to move Pieces, and click them once to highlight where they can move.

Note that the game can only be run from this directory, as it contains it's assets.

![](screenshot.png)
