use anyhow::*;
use image::GenericImageView;
use image::Pixel;
use lazy_static::*;
use minifb::{Key, MouseButton, MouseMode, Scale, Window, WindowOptions};
use rand::*;
use rayon::prelude::*;
use std::collections::HashMap;

macro_rules! row{
    ($color: expr => $($piece: expr),*) => {
        [$(Some(Block::new($piece, $color))),*]
    }
}

lazy_static! {
    static ref ASSETS: HashMap<String, [u32; SQUARE_SIZE * SQUARE_SIZE]> = {
        let mut tmp = HashMap::new();

        const NEEDED: &'static [&str] = &[
            "grass_tile",
            "Pawn",
            "Bishop",
            "King",
            "Knight",
            "Queen",
            "Rook",
        ];

        for asset in NEEDED {
            let mut buffer = [0u32; SQUARE_SIZE * SQUARE_SIZE];
            let image = image::open(&format!("assets/{}.png", asset)).unwrap();

            let dim = image.dimensions();

            for x in 0..dim.0 as u32 {
                for y in 0..dim.1 as u32 {
                    let v = image.get_pixel(x, y).to_rgb();
                    buffer[x as usize + y as usize * SQUARE_SIZE] =
                        ((v[0] as u32) << 16) | ((v[1] as u32) << 8) | v[2] as u32;
                }
            }

            tmp.insert(asset.to_string(), buffer);
        }

        tmp
    };
    static ref START_BOARD: Board = {
        let mut a = row![0 => Rook, Knight, Bishop, Queen, King, Bishop, Knight, Rook];
        let mut b = [None; BOARD_SIZE];
        let mut c = [None; BOARD_SIZE];

        for n in 0..a.len() {
            b[n] = a[n];
            if let Some(a) = &mut a[n] {
                a.color = WHITE
            };
            c[n] = a[n];
        }

        let mut tmp = Board([[None; BOARD_SIZE]; BOARD_SIZE]);
        tmp.0[1] = [Some(Block::new(Pawn, 0)); BOARD_SIZE];
        tmp.0[tmp.0.len() - 2] = [Some(Block::new(Pawn, WHITE)); BOARD_SIZE];

        tmp.0[0] = b;
        tmp.0[tmp.0.len() - 1] = c;

        tmp
    };
}

//const BOARD_SIZE: usize = 10;
//const SQUARE_SIZE: usize = SIZE / BOARD_SIZE;
//const SIZE: usize = 250;

const BOARD_SIZE: usize = 8;
const SQUARE_SIZE: usize = 25;
const SIZE: usize = SQUARE_SIZE * BOARD_SIZE;

const SIZE_SQ: usize = SIZE * SIZE;

const WHITE: u32 = 0x00dddddd;
const DARK_RED: u32 = 0x00b82517;
const RED: u32 = 0x00eb1f1f;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum Piece {
    King,
    Queen,
    Pawn,
    Rook,
    Bishop,
    Knight,
}

use Piece::*;

impl Piece {
    fn can_move_to(&self, sx: usize, sy: usize, ox: usize, oy: usize) -> bool {
        let sx = sx as isize;
        let ox = ox as isize;
        let oy = oy as isize;
        let sy = sy as isize;

        match self {
            King => (sx - ox).abs() < 2 && (sy - oy).abs() < 2,
            Rook => sx == ox || sy == oy,
            Bishop => (sx - ox).abs() == (sy - oy).abs(),
            Knight => {
                ((sx - ox).abs() == 2 && (sy - oy).abs() == 1)
                    || ((sx - ox).abs() == 1 && (sy - oy).abs() == 2)
            }
            Pawn => {
                (sx == ox && (sy - oy).abs() < 2) || ((sx - ox).abs() < 2 && (sy - oy).abs() == 1)
            }
            Queen => ((sx - ox).abs() == (sy - oy).abs()) || (sx == ox || sy == oy),
        }
    }
}

#[derive(Clone, Copy, Debug)]
struct Block {
    piece: Piece,
    sprite: [u32; SQUARE_SIZE * SQUARE_SIZE],
    color: u32,
}

impl Block {
    fn new(piece: Piece, color: u32) -> Self {
        Self {
            piece,
            color,
            sprite: ASSETS[&format!("{:#?}", piece)],
        }
    }
}

#[derive(Clone, Copy)]
struct Board([[Option<Block>; BOARD_SIZE]; BOARD_SIZE]);

impl Board {
    fn set_pixel(&self, x: usize, y: usize, n: usize, o: &mut u32) {
        if let Some(piece) = self.0[x][y] {
            //let name = format!("{:#?}", piece.piece);
            if piece.sprite[n] != 0x004cab50 {
                *o = if piece.sprite[n] == 0 {
                    if piece.color == 0 {
                        return;
                    }
                    piece.color
                } else {
                    piece.sprite[n]
                }
            }
        }
    }

    fn paint_on_sprite(&mut self, x: usize, y: usize, n: usize) -> Option<&mut u32> {
        if let Some(piece) = &mut self.0[x][y] {
            if piece.sprite[n] != 0x004cab50 && piece.sprite[n] != 0 {
                return Some(&mut piece.sprite[n]);
            }
        }
        None
    }
}

//fn change_color(board: &mut Board, old_color: u32, new_color: u32) {
//    board.0.par_iter_mut().for_each(|row| {
//        row.par_iter_mut().for_each(|block| {
//            if let Some(piece) = block {
//                if piece.color == old_color {
//                    piece.color = new_color
//                }
//            }
//        })
//    });
//}

#[derive(Clone, Copy)]
struct GameState {
    background: [u32; SIZE_SQ],
    board: Board,
    buffer: [u32; SIZE_SQ],
    undo_buffer: Board,
}

impl GameState {
    fn rebuffer(&mut self) {
        self.buffer = self.background;
        let board = &self.board;
        self.buffer.par_iter_mut().enumerate().for_each(|(n, o)| {
            let x = n / SIZE / SQUARE_SIZE;
            let y = n % SIZE / SQUARE_SIZE;
            let tile_n = n % SIZE % SQUARE_SIZE + (n / SIZE % SQUARE_SIZE) * SQUARE_SIZE;

            board.set_pixel(x, y, tile_n, o);
        });
    }

    fn highlight_moves(&mut self, sx: usize, sy: usize) {
        self.buffer = self.background;
        let board = &self.board;

        let piece = if let Some(b) = self.board.0[sy][sx] {
            b.piece
        } else {
            return;
        };

        self.buffer.par_iter_mut().enumerate().for_each(|(n, o)| {
            let x = n / SIZE / SQUARE_SIZE;
            let y = n % SIZE / SQUARE_SIZE;
            let tile_n = n % SIZE % SQUARE_SIZE + (n / SIZE % SQUARE_SIZE) * SQUARE_SIZE;

            if piece.can_move_to(sx, sy, y, x) {
                *o += 0x00000080
            }

            board.set_pixel(x, y, tile_n, o)
        });
    }

    fn apply_effect(&mut self, x: usize, y: usize, effect: fn(usize, u32) -> u32, to_fg: bool) {
        for nx in 0..SQUARE_SIZE {
            for ny in 0..SQUARE_SIZE {
                let tile_n = nx + ny * SQUARE_SIZE;
                let n = x * SQUARE_SIZE + y * SQUARE_SIZE * SIZE + nx + ny * SIZE;

                if !to_fg {
                    self.buffer[n] = effect(tile_n, self.background[n])
                }

                if self.board.0[y][x].is_some()
                    && ASSETS[&format!("{:#?}", self.board.0[y][x].unwrap().piece)][tile_n]
                        != 0x004cab50
                {
                    let mut p = 0;
                    self.board.set_pixel(y, x, tile_n, &mut p);
                    if to_fg {
                        self.buffer[n] = effect(tile_n, p)
                    } else {
                        self.buffer[n] = p
                    }
                }
            }
        }
    }

    fn paint_block(&mut self, l: Layer, x: usize, y: usize, effect: fn(usize, usize, u32) -> u32) {
        for nx in 0..SQUARE_SIZE {
            for ny in 0..SQUARE_SIZE {
                let tile_n = nx + ny * SQUARE_SIZE;
                let n = x * SQUARE_SIZE + y * SQUARE_SIZE * SIZE + nx + ny * SIZE;

                match l {
                    Layer::Fg => {
                        if let Some(p) = self.board.paint_on_sprite(y, x, tile_n) {
                            *p = effect(nx, ny, *p)
                        }
                    }
                    Layer::Bg => self.background[n] = effect(nx, ny, self.background[n]),
                    Layer::Overlay => {
                        let p = self
                            .board
                            .paint_on_sprite(y, x, tile_n)
                            .unwrap_or(&mut self.background[n]);
                        *p = effect(nx, ny, *p)
                    }
                    Layer::All => {
                        if let Some(p) = self.board.paint_on_sprite(y, x, tile_n) {
                            *p = effect(nx, ny, *p)
                        }
                        self.background[n] = effect(nx, ny, self.background[n])
                    }
                }
            }
        }
    }
}

#[derive(Clone, Copy, Debug)]
enum Layer {
    Fg,
    Bg,
    Overlay,
    All,
}

fn draw_box_on_block(buffer: &mut [u32; SIZE_SQ], x: usize, y: usize) {
    for x in (x * SQUARE_SIZE).max(1) - 1..((x + 1) * SQUARE_SIZE + 1).min(SIZE - 1) {
        buffer[x + ((y * SQUARE_SIZE).max(2) - 2) * SIZE] = WHITE;
        buffer[x + (((y + 1).min(BOARD_SIZE - 1) * SQUARE_SIZE).min(SIZE - 1) + 1) * SIZE] = WHITE;
    }

    for y in (y * SQUARE_SIZE).max(1) - 1..((y + 1) * SQUARE_SIZE + 1).min(SIZE - 1) {
        buffer[((x * SQUARE_SIZE).max(2) - 2) + y * SIZE] = WHITE;
        buffer[(((x + 1).min(BOARD_SIZE - 1) * SQUARE_SIZE).min(SIZE - 1) + 1) + y * SIZE] = WHITE;
    }
}

fn noise(x: usize, y: usize, x_: bool, y_: bool, abs: bool, scale: f32) -> bool {
    const P: f32 = SQUARE_SIZE as f32 / 2.;
    let mut x = (x as f32 - P) / P;
    let mut y = (y as f32 - P) / P;

    if abs {
        x = x.abs();
        y = y.abs();
    }

    let is_x = if x_ {
        x < random::<f32>() * scale
    } else {
        x > random::<f32>() * scale
    };

    let is_y = if y_ {
        y < random::<f32>() * scale
    } else {
        y > random::<f32>() * scale
    };

    is_x && is_y
}

fn main() -> Result<()> {
    let mut window = Window::new(
        "Bloody Skak",
        SIZE,
        SIZE,
        WindowOptions {
            scale: Scale::X4,
            ..WindowOptions::default()
        },
    )?;

    let mut background = [0; SIZE_SQ];

    background.par_iter_mut().enumerate().for_each(|(n, o)| {
        let block_x = n / SIZE / SQUARE_SIZE;
        let block_y = n % SIZE / SQUARE_SIZE;
        let tile_n = n % SIZE % SQUARE_SIZE + (n / SIZE % SQUARE_SIZE) * SQUARE_SIZE;

        *o = if (block_x + block_y) % 2 == 0 {
            ASSETS["grass_tile"][tile_n] + 0x00111111
        } else {
            ASSETS["grass_tile"][tile_n].max(0x00111111) - 0x00111111
        }
    });

    let mut game = GameState {
        board: *START_BOARD,
        background,
        buffer: background,
        undo_buffer: *START_BOARD,
    };

    drop(background);
    game.rebuffer();

    let mut is_highlighted = false;
    let mut highlighted_x = 0;
    let mut highlighted_y = 0;

    while window.is_open() && !window.is_key_down(Key::Q) {
        let mut postbuffer = game.buffer.clone();
        window.get_mouse_pos(MouseMode::Discard).map(|(x, y)| {
            let x = x as usize / SQUARE_SIZE;
            let y = y as usize / SQUARE_SIZE;

            draw_box_on_block(&mut postbuffer, x, y);

            if !is_highlighted
                && window.get_mouse_down(MouseButton::Left)
                && game.board.0[y][x].is_some()
            {
                game.highlight_moves(x, y);
                game.apply_effect(x, y, |_, p| p + 0x00000080, true);
                highlighted_x = x;
                highlighted_y = y;
                is_highlighted = true;
            } else if is_highlighted
                && !window.get_mouse_down(MouseButton::Left)
                && (highlighted_x != x || highlighted_y != y)
                && game.board.0[highlighted_y][highlighted_x]
                    .unwrap()
                    .piece
                    .can_move_to(x, y, highlighted_x, highlighted_y)
            {
                game.undo_buffer = game.board;
                let was_some = game.board.0[y][x].is_some();
                game.board.0[y][x] = game.board.0[highlighted_y][highlighted_x];
                game.board.0[highlighted_y][highlighted_x] = None;

                macro_rules! spray_blood {
                    (
                        $layer: ident,
                        $x: expr,
                        $y: expr,
                        |$x_fg:ident,$y_fg:ident|$noise_fg: expr) => {{
                        game.paint_block(
                            Layer::$layer,
                            ($x).min(BOARD_SIZE - 1),
                            ($y).min(BOARD_SIZE - 1),
                            |$x_fg, $y_fg, p| {
                                if $noise_fg {
                                    if random() {
                                        DARK_RED
                                    } else {
                                        RED
                                    }
                                } else {
                                    p
                                }
                            },
                        );
                    }};
                }

                if was_some {
                    spray_blood!(Overlay, x + 1, y.max(1) - 1, |x, y| noise(
                        x,
                        SQUARE_SIZE - y,
                        true,
                        true,
                        false,
                        -1.
                    ));

                    spray_blood!(Overlay, x, y.max(1) - 1, |x, y| noise(
                        x, y, true, false, false, 1.5
                    ));

                    spray_blood!(Bg, x, y, |x, y| noise(x, y, false, false, true, 1.));

                    spray_blood!(Bg, x, y, |_x, _y| random());

                    spray_blood!(Fg, x, y, |x, y| noise(x, y, true, false, false, 1.5));

                    spray_blood!(Overlay, x.max(1) - 1, y.max(1) - 1, |x, y| noise(
                        x, y, false, false, false, 1.5
                    ));

                    spray_blood!(Bg, x + 1, y + 1, |x, y| noise(x, y, true, true, false, -1.));

                    spray_blood!(Bg, x, y + 1, |x, y| noise(
                        x,
                        SQUARE_SIZE - y,
                        true,
                        false,
                        false,
                        1.
                    ));

                    spray_blood!(All, x + 1, y, |x, y| noise(
                        SQUARE_SIZE - x,
                        y,
                        false,
                        true,
                        false,
                        1.5
                    ));

                    spray_blood!(All, x.max(1) - 1, y, |x, y| noise(
                        x, y, false, true, false, 1.5
                    ));

                    spray_blood!(Bg, x.max(1) - 1, y + 1, |x, y| noise(
                        SQUARE_SIZE - x,
                        y,
                        true,
                        true,
                        false,
                        -1.
                    ));
                }

                game.rebuffer();
                is_highlighted = false;
            }

            if is_highlighted && !window.get_mouse_down(MouseButton::Left) {
                is_highlighted = false;
            }
        });

        if window.is_key_pressed(Key::U, minifb::KeyRepeat::No) {
            game.board = game.undo_buffer;
            game.rebuffer();
        }

        window.update_with_buffer(&postbuffer, SIZE, SIZE)?;
    }

    Ok(())
}
